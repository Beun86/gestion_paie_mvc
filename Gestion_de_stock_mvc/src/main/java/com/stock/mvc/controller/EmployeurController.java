package com.stock.mvc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entity.Employeur;
import com.stock.mvc.entity.Salarie;
import com.stock.mvc.service.IEmployeurService;
import com.stock.mvc.service.ISalarieService;
@Controller
public class EmployeurController {
private static final Logger logger = LoggerFactory.getLogger(EmployeurController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
@Autowired
private IEmployeurService employeurservice;
@RequestMapping(value = "/employeur", method = RequestMethod.GET)
public String employeur(Locale locale, Model model) {
	
	List<Employeur> employeurs = employeurservice.selectAll();
	if(employeurs == null) {
		employeurs = new ArrayList<Employeur>();
	}
	model.addAttribute("employeurs",employeurs);
	return "employeur/employeur";
}
}
