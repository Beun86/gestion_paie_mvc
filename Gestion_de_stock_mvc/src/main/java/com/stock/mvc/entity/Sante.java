package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="SANTE")
public class Sante implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IDSANTE")
	private Long idSante;
	@Column(name="SECUSOCIALE")
	private BigDecimal secuSociale;
	@Column(name="COMPLETA")
	private BigDecimal compleTA;
	@Column(name="COMPLETB")
	private BigDecimal compleTB;
	@Column(name="COMPLESANTE")
	private BigDecimal compleSante;
	@Column(name="MALADIEAT")
	private BigDecimal maladieAT;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="sante_idSalaire",unique=true)
	private Salaire salaire;

	public Long getIdSante() {
		return idSante;
	}

	public void setIdSante(Long idSante) {
		this.idSante = idSante;
	}

	public BigDecimal getSecuSociale() {
		return secuSociale;
	}

	public void setSecuSociale(BigDecimal secuSociale) {
		this.secuSociale = secuSociale;
	}

	public BigDecimal getCompleTA() {
		return compleTA;
	}

	public void setCompleTA(BigDecimal compleTA) {
		this.compleTA = compleTA;
	}

	public BigDecimal getCompleTB() {
		return compleTB;
	}

	public void setCompleTB(BigDecimal compleTB) {
		this.compleTB = compleTB;
	}

	public BigDecimal getCompleSante() {
		return compleSante;
	}

	public void setCompleSante(BigDecimal compleSante) {
		this.compleSante = compleSante;
	}

	public BigDecimal getMaladieAT() {
		return maladieAT;
	}

	public void setMaladieAT(BigDecimal maladieAT) {
		this.maladieAT = maladieAT;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}

	public Sante(BigDecimal secuSociale, BigDecimal compleTA, BigDecimal compleTB, BigDecimal compleSante,
			BigDecimal maladieAT, Salaire salaire) {
		super();
		this.secuSociale = secuSociale;
		this.compleTA = compleTA;
		this.compleTB = compleTB;
		this.compleSante = compleSante;
		this.maladieAT = maladieAT;
		this.salaire = salaire;
	}

	public Sante() {
		super();
	}
	
	
}
