package com.stock.mvc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="EMPLOYEUR")
public class Employeur implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1911630492706518164L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name="IDEMPLOYEUR")
	private int idEmployeur;
	@Column(name="RAISONSOCIALE")
	private String raisonSociale;
	@Column(name="SIRET")
	private String siret;
	@Column(name="CODEAPE")
	private String codeAPE;
	
	@OneToMany(mappedBy ="idEmployeur")
	private List<Salaire> salaire;
	
	public int getId() {
		return idEmployeur;
	}
	public void setId(int idEmployeur) {
		this.idEmployeur = idEmployeur;
	}
	public String getRaisonSociale() {
		return raisonSociale;
	}
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public String getCodeAPE() {
		return codeAPE;
	}
	public void setCodeAPE(String codeAPE) {
		this.codeAPE = codeAPE;
	}
	public Employeur(String raisonSociale, String siret, String codeAPE) {
		super();
		this.raisonSociale = raisonSociale;
		this.siret = siret;
		this.codeAPE = codeAPE;
	}
	public Employeur() {
		super();
	}
	

}
 