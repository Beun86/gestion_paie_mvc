package com.stock.mvc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SALARIE")
public class Salarie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IDSALARIE")
	private Long idSalarie;
	@Column(name="NOM")
	private String nom;
	@Column(name="PRENOM")
	private String prenom;
	@Column(name="NUMSECU")
	private int numSecu;
	@ManyToOne
	@JoinColumn(name="idEmployeur")
	private Employeur emp1;
	
	public Long getId() {
		return idSalarie;
	}
	public void setId(Long idSalarie) {
		this.idSalarie = idSalarie;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getNumSecu() {
		return numSecu;
	}
	public void setNumSecu(int numSecu) {
		this.numSecu = numSecu;
	}
	public Salarie(String nom, String prenom, int numSecu) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numSecu = numSecu;
	}
	public Salarie() {
		super();
	}

}
