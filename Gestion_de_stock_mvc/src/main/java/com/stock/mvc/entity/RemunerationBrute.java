package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="REMUNERATIONBRUTE")
public class RemunerationBrute implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IDREMUNERATIONBRUTE")
	private Long idRemunerationBrute;
	@Column(name="SALAIREBASE")
	private BigDecimal salaireBase;
	@Column(name="ABSENCENR")
	private BigDecimal absenceNR;
	@Column(name="HEURESUPP")
	private BigDecimal heureSupp;
	@Column(name="INDEMNITENS")
	private BigDecimal indemniteNS;
	@Column(name="PRIME")
	private BigDecimal prime;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="remunerationBrute_idSalaire",unique=true)
	private Salaire salaire;

	public Long getIdRemunerationBrute() {
		return idRemunerationBrute;
	}

	public void setIdRemunerationBrute(Long idRemunerationBrute) {
		this.idRemunerationBrute = idRemunerationBrute;
	}

	public BigDecimal getSalaireBase() {
		return salaireBase;
	}

	public void setSalaireBase(BigDecimal salaireBase) {
		this.salaireBase = salaireBase;
	}

	public BigDecimal getAbsenceNR() {
		return absenceNR;
	}

	public void setAbsenceNR(BigDecimal absenceNR) {
		this.absenceNR = absenceNR;
	}

	public BigDecimal getHeureSupp() {
		return heureSupp;
	}

	public void setHeureSupp(BigDecimal heureSupp) {
		this.heureSupp = heureSupp;
	}

	public BigDecimal getIndemniteNS() {
		return indemniteNS;
	}

	public void setIndemniteNS(BigDecimal indemniteNS) {
		this.indemniteNS = indemniteNS;
	}

	public BigDecimal getPrime() {
		return prime;
	}

	public void setPrime(BigDecimal prime) {
		this.prime = prime;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}

	public RemunerationBrute(BigDecimal salaireBase, BigDecimal absenceNR, BigDecimal heureSupp, BigDecimal indemniteNS,
			BigDecimal prime, Salaire salaire) {
		super();
		this.salaireBase = salaireBase;
		this.absenceNR = absenceNR;
		this.heureSupp = heureSupp;
		this.indemniteNS = indemniteNS;
		this.prime = prime;
		this.salaire = salaire;
	}

	public RemunerationBrute() {
		super();
	}

	
}
